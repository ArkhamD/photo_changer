package com.example.task_1

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button
import android.widget.ImageView
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var photos: ImageView
    private lateinit var btn_changePhoto: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        photos = findViewById(R.id.photos)
        btn_changePhoto = findViewById(R.id.btn_changePhoto)

        btn_changePhoto.setOnClickListener(object: OnClickListener {
            override fun onClick(p0: View?) {
                when ((1..7).random()) {
                    1 -> photos.setImageResource(R.drawable.a1)
                    2 -> photos.setImageResource(R.drawable.a2)
                    3 -> photos.setImageResource(R.drawable.a3)
                    4 -> photos.setImageResource(R.drawable.a4)
                    5 -> photos.setImageResource(R.drawable.a5)
                    6 -> photos.setImageResource(R.drawable.a6)
                    else -> photos.setImageResource(R.drawable.a7)
                }
                when ((1..4).random()) {
                    1 -> btn_changePhoto.setBackgroundColor(Color.RED)
                    2 -> btn_changePhoto.setBackgroundColor(Color.BLUE)
                    3 -> btn_changePhoto.setBackgroundColor(Color.GREEN)
                    else -> btn_changePhoto.setBackgroundColor(Color.GRAY)
                }
            }

        })
    }
}